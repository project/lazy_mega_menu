/**
 *
 */
(function ($, Drupal, window, document) {

  Drupal.behaviors.lazy_mega_menu = {
    attach: function (context, settings) {
      if ($('.responsive-menu-block-wrapper').once('megaMenuContent').is(':visible')) {
        // Add content to Get Inspired menu. Get content from REST view.
        $.getJSON('/mega-menu-content', function (data) {
          $.each(data, function (i, item) {
            // Make the menu change.
            $('ul.horizontal-menu a[href$="' + item.menu_path + '"]')
              .next()
              .append('<div class="lazy-mega-menu">' + item.content + '</div>');
          })
        });
      }
    }
  };

} (jQuery, Drupal, this, this.document));
