<?php

namespace Drupal\lazy_mega_menu\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\lazy_mega_menu\Entity\MegaMenuInterface;

/**
 * Class MegaMenuController.
 *
 *  Returns responses for Mega Menu Content routes.
 *
 * @package Drupal\lazy_mega_menu\Controller
 */
class MegaMenuController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Mega Menu Content  revision.
   *
   * @param int $mega_menu_revision
   *   The Mega Menu Content  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($mega_menu_revision) {
    $entity_type_manager = static::entityTypeManager();
    $mega_menu = $entity_type_manager->getStorage('mega_menu')->loadRevision($mega_menu_revision);
    $view_builder = $entity_type_manager->getViewBuilder('mega_menu');

    return $view_builder->view($mega_menu);
  }

  /**
   * Page title callback for a Mega Menu Content  revision.
   *
   * @param int $mega_menu_revision
   *   The Mega Menu Content revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($mega_menu_revision) {
    /* @var \Drupal\lazy_mega_menu\Entity\MegaMenuInterface $mega_menu */
    $mega_menu = static::entityTypeManager()->getStorage('mega_menu')->loadRevision($mega_menu_revision);
    /* @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = \Drupal::service('date.formatter');
    return $this->t('Revision of %title from %date', ['%title' => $mega_menu->label(), '%date' => $date_formatter->format($mega_menu->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Mega Menu Content .
   *
   * @param \Drupal\lazy_mega_menu\Entity\MegaMenuInterface $mega_menu
   *   A Mega Menu Content  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(MegaMenuInterface $mega_menu) {
    $account = $this->currentUser();
    $langcode = $mega_menu->language()->getId();
    $langname = $mega_menu->language()->getName();
    $languages = $mega_menu->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    /* @var \Drupal\lazy_mega_menu\MegaMenuStorageInterface $mega_menu_storage */
    $mega_menu_storage = static::entityTypeManager()->getStorage('mega_menu');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $mega_menu->label()]) : $this->t('Revisions for %title', ['%title' => $mega_menu->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all mega menu content revisions") || $account->hasPermission('administer mega menu content entities')));
    $delete_permission = (($account->hasPermission("delete all mega menu content revisions") || $account->hasPermission('administer mega menu content entities')));

    $rows = [];

    $vids = $mega_menu_storage->revisionIds($mega_menu);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\lazy_mega_menu\Entity\MegaMenuInterface $revision */
      $revision = $mega_menu_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $mega_menu->getRevisionId()) {
          $link = Link::createFromRoute($date, 'entity.mega_menu.revision', ['mega_menu' => $mega_menu->id(), 'mega_menu_revision' => $vid])->toString();
        }
        else {
          $link = Link::createFromRoute($date, 'entity.mega_menu.canonical', ['mega_menu' => $mega_menu->id()])->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $revert_route_parameters = [
              'mega_menu' => $mega_menu->id(),
              'mega_menu_revision' => $vid,
            ];
            if ($has_translations) {
              $revert_route_name = 'entity.mega_menu.translation_revert';
              $revert_route_parameters['langcode'] = $langcode;
            }
            else {
              $revert_route_name = 'entity.mega_menu.revision_revert';
            }

            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute($revert_route_name, $revert_route_parameters),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.mega_menu.revision_delete', ['mega_menu' => $mega_menu->id(), 'mega_menu_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['mega_menu_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
