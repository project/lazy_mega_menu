<?php

namespace Drupal\lazy_mega_menu;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Mega Menu Content entity.
 *
 * @see \Drupal\lazy_mega_menu\Entity\MegaMenu.
 */
class MegaMenuAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\lazy_mega_menu\Entity\MegaMenuInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished mega menu content entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published mega menu content entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit mega menu content entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete mega menu content entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add mega menu content entities');
  }

}
