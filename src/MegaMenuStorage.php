<?php

namespace Drupal\lazy_mega_menu;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\lazy_mega_menu\Entity\MegaMenuInterface;

/**
 * Defines the storage handler class for Mega Menu Content entities.
 *
 * This extends the base storage class, adding required special handling for
 * Mega Menu Content entities.
 *
 * @ingroup lazy_mega_menu
 */
class MegaMenuStorage extends SqlContentEntityStorage implements MegaMenuStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(MegaMenuInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {mega_menu_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {mega_menu_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(MegaMenuInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {mega_menu_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('mega_menu_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
