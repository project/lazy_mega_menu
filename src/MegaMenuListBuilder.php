<?php

namespace Drupal\lazy_mega_menu;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Mega Menu Content entities.
 *
 * @ingroup lazy_mega_menu
 */
class MegaMenuListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Mega Menu Content ID');
    $header['path'] = $this->t('Menu Path');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\lazy_mega_menu\Entity\MegaMenu */
    $row['id'] = $entity->id();
    $row['path'] = Link::createFromRoute($entity->getName(), 'entity.mega_menu.edit_form', ['mega_menu' => $entity->id()]);
    return $row + parent::buildRow($entity);
  }

}
