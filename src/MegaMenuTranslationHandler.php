<?php

namespace Drupal\lazy_mega_menu;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for mega_menu.
 */
class MegaMenuTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
