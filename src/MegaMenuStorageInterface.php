<?php

namespace Drupal\lazy_mega_menu;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\lazy_mega_menu\Entity\MegaMenuInterface;

/**
 * Defines the storage handler class for Mega Menu Content entities.
 *
 * This extends the base storage class, adding required special handling for
 * Mega Menu Content entities.
 *
 * @ingroup lazy_mega_menu
 */
interface MegaMenuStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Mega Menu Content revision IDs for a specific entity.
   *
   * @param \Drupal\lazy_mega_menu\Entity\MegaMenuInterface $entity
   *   The Mega Menu Content entity.
   *
   * @return int[]
   *   Mega Menu Content revision IDs (in ascending order).
   */
  public function revisionIds(MegaMenuInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as an author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Mega Menu Content revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\lazy_mega_menu\Entity\MegaMenuInterface $entity
   *   The Mega Menu Content entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(MegaMenuInterface $entity);

  /**
   * Unsets the language for all Mega Menu Content with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
