<?php

namespace Drupal\lazy_mega_menu\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Mega Menu Content revision.
 *
 * @ingroup lazy_mega_menu
 */
class MegaMenuRevisionDeleteForm extends ConfirmFormBase {


  /**
   * The Mega Menu Content revision.
   *
   * @var \Drupal\lazy_mega_menu\Entity\MegaMenuInterface
   */
  protected $revision;

  /**
   * The Mega Menu Content storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $MegaMenuStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface;
   */
  protected $dateFormatter;

  /**
   * Constructs a new MegaMenuRevisionDeleteForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityStorageInterface $entity_storage, Connection $connection, DateFormatterInterface $date_formatter) {
    $this->MegaMenuStorage = $entity_storage;
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager->getStorage('mega_menu'),
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mega_menu_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $revision_date = $this->dateFormatter->format($this->revision->getRevisionCreationTime());
    return t('Are you sure you want to delete the revision from %revision-date?', ['%revision-date' => $revision_date]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.mega_menu.version_history', ['mega_menu' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $mega_menu_revision = NULL) {
    $this->revision = $this->MegaMenuStorage->loadRevision($mega_menu_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->MegaMenuStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Mega Menu Content: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $revision_date = $this->dateFormatter->format($this->revision->getRevisionCreationTime());
    drupal_set_message(t('Revision from %revision-date of Mega Menu Content %title has been deleted.', ['%revision-date' => $revision_date, '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.mega_menu.canonical',
       ['mega_menu' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {mega_menu_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.mega_menu.version_history',
         ['mega_menu' => $this->revision->id()]
      );
    }
  }

}
