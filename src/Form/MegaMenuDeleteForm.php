<?php

namespace Drupal\lazy_mega_menu\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Mega Menu Content entities.
 *
 * @ingroup lazy_mega_menu
 */
class MegaMenuDeleteForm extends ContentEntityDeleteForm {


}
