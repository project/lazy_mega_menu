<?php

namespace Drupal\lazy_mega_menu\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Mega Menu Content entities.
 */
class MegaMenuViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
