<?php

namespace Drupal\lazy_mega_menu\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Mega Menu Content entity.
 *
 * @ingroup lazy_mega_menu
 *
 * @ContentEntityType(
 *   id = "mega_menu",
 *   label = @Translation("Mega Menu Content"),
 *   handlers = {
 *     "storage" = "Drupal\lazy_mega_menu\MegaMenuStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lazy_mega_menu\MegaMenuListBuilder",
 *     "views_data" = "Drupal\lazy_mega_menu\Entity\MegaMenuViewsData",
 *     "translation" = "Drupal\lazy_mega_menu\MegaMenuTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\lazy_mega_menu\Form\MegaMenuForm",
 *       "add" = "Drupal\lazy_mega_menu\Form\MegaMenuForm",
 *       "edit" = "Drupal\lazy_mega_menu\Form\MegaMenuForm",
 *       "delete" = "Drupal\lazy_mega_menu\Form\MegaMenuDeleteForm",
 *     },
 *     "access" = "Drupal\lazy_mega_menu\MegaMenuAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\lazy_mega_menu\MegaMenuHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "mega_menu",
 *   data_table = "mega_menu_field_data",
 *   revision_table = "mega_menu_revision",
 *   revision_data_table = "mega_menu_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer mega menu content",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "path" = "path",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/mega_menu/{mega_menu}",
 *     "add-form" = "/admin/content/mega_menu/add",
 *     "edit-form" = "/admin/content/mega_menu/{mega_menu}/edit",
 *     "delete-form" = "/admin/content/mega_menu/{mega_menu}/delete",
 *     "version-history" = "/admin/content/mega_menu/{mega_menu}/revisions",
 *     "revision" = "/admin/content/mega_menu/{mega_menu}/revisions/{mega_menu_revision}/view",
 *     "revision_revert" = "/admin/content/mega_menu/{mega_menu}/revisions/{mega_menu_revision}/revert",
 *     "translation_revert" = "/admin/content/mega_menu/{mega_menu}/revisions/{mega_menu_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/content/mega_menu/{mega_menu}/revisions/{mega_menu_revision}/delete",
 *     "collection" = "/admin/content/mega_menu",
 *   },
 *   field_ui_base_route = "mega_menu.settings"
 * )
 */
class MegaMenu extends RevisionableContentEntityBase implements MegaMenuInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the mega_menu owner
    // the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('menu_path')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('menu_path', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Mega Menu Content entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['menu_path'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Menu Path'))
      ->setDescription(t('The path of the menu item this mega menu content should attach to.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'type' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Mega Menu Content is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['content'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Content'))
      ->setDescription(t('Please enter the content you want to display on this mega menu.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 1,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textarea_with_summary',
        'weight' => 1,
      ));

    return $fields;
  }

}
