<?php

namespace Drupal\lazy_mega_menu\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Mega Menu Content entities.
 *
 * @ingroup lazy_mega_menu
 */
interface MegaMenuInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface, EntityInterface, TranslatableInterface, ContentEntityInterface {

  /**
   * Gets the Mega Menu Content name.
   *
   * @return string
   *   Name of the Mega Menu Content.
   */
  public function getName();

  /**
   * Sets the Mega Menu Content name.
   *
   * @param string $name
   *   The Mega Menu Content name.
   *
   * @return \Drupal\lazy_mega_menu\Entity\MegaMenuInterface
   *   The called Mega Menu Content entity.
   */
  public function setName($name);

  /**
   * Gets the Mega Menu Content creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Mega Menu Content.
   */
  public function getCreatedTime();

  /**
   * Sets the Mega Menu Content creation timestamp.
   *
   * @param int $timestamp
   *   The Mega Menu Content creation timestamp.
   *
   * @return \Drupal\lazy_mega_menu\Entity\MegaMenuInterface
   *   The called Mega Menu Content entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Mega Menu Content published status indicator.
   *
   * Unpublished Mega Menu Content are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Mega Menu Content is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Mega Menu Content.
   *
   * @param bool $published
   *   TRUE to set this Mega Menu Content to published.
   *   FALSE to set it to unpublished.
   *
   * @return \Drupal\lazy_mega_menu\Entity\MegaMenuInterface
   *   The called Mega Menu Content entity.
   */
  public function setPublished($published);

  /**
   * Gets the Mega Menu Content revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Mega Menu Content revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\lazy_mega_menu\Entity\MegaMenuInterface
   *   The called Mega Menu Content entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Mega Menu Content revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Mega Menu Content revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\lazy_mega_menu\Entity\MegaMenuInterface
   *   The called Mega Menu Content entity.
   */
  public function setRevisionUserId($uid);

}
