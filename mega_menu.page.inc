<?php

/**
 * @file
 * Contains mega_menu.page.inc.
 *
 * Page callback for Mega Menu Content entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Mega Menu Content templates.
 *
 * Default template: mega_menu.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_mega_menu(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
