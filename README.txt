Lazy Mega Menu
==============

This module will help you add content to top level menu items when using drupal/responsive_menu


Things to note
==============

* You MUST use the responsive_menu horizontal menu to display your menu items.
* You MUST have sub menu items below the menu item you are adding a mega-menu to.
